module nstall.info;
import std.string: indexOf, split, strip, splitLines;
import std.stdio: File;
import std.conv: to;

// Publicly imported to simplify responding to throws from Parse()
public import std.exception: ErrnoException;

/++
	Implements parser and storage for info data.
++/

/*
	Format is:
	key : val

	where ':' is the delimiter
	key and val will have surrounding whitespace culled via .strip()
	any non-valid lines will be ignored if strict is false
*/

/// Error thrown if parsing fails.
class ParserError: Exception
{
	@safe nothrow this(string msg)
	{
		super(msg);
	}
}

/++
	Struct containing results from scanning an info file.
++/
struct Info
{
	string name;       // Name of the package
	string type;       // What kind of package it is, mainly for front-end tools to work with
	string maintainer; // Maintainers of the package
	string edition;	// Version of the package
	string toString()
	{
		return "name: " ~ name ~ "\ntype: " ~ type ~ "\nmaintainer: " ~ maintainer ~ "\nversion: " ~ edition;
	}
}

/++
	Parse a target info file into a valid Info struct.
	Notes:
		If strict is false then any non-valid lines will simply be ignored.
	Takes:
		string path - path to the info file
		bool strict = true - control whether to throw on a malformed line
	Returns:
		Info rhs - Info struct populated with data
	Throws:
		ErrnoException - if target file does not exist
		ParserError - if target file cannot be properly parsed
++/
Info ParseInfoFile(string path, bool strict = false)
{
	Info rhs;
	File fp = File(path);

	size_t lineNo = 0;

	foreach (line; fp.byLine())
	{
		lineNo++;
		if (line.indexOf(':') != -1)
		{
			string key, val;
			if (line.split(":").length < 2) throw new ParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
			key = to!string(line.split(":")[0]).strip();
			val = to!string(line.split(":")[1]).strip();

			switch (key)
			{
				case "name": rhs.name = val; break;
				case "type": rhs.type = val; break;
				case "maintainer": rhs.maintainer = val; break;
				case "version": rhs.edition = val; break;
				default: if (strict) throw new ParserError("Invalid data on line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line)); break;
			}
		}
		else
			if (strict) throw new ParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
	}

	return rhs;
}

/++
	Parse a string of data into a valid Info struct.
	Notes:
		If strict is false then any non-valid lines will simply be ignored.
	Takes:
		string data - data to parse
		bool strict = true - control whether to throw on a malformed line
	Returns:
		Info rhs - Info struct populated with data
	Throws:
		ParserError - if target file cannot be properly parsed
++/
Info ParseInfoData(string data, bool strict = false)
{
	Info rhs;
	size_t lineNo = 0;

	foreach (line; data.splitLines())
	{
		lineNo++;
		if (line.indexOf(':') != -1)
		{
			string key, val;
			if (line.split(":").length < 2) throw new ParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
			key = to!string(line.split(":")[0]).strip();
			val = to!string(line.split(":")[1]).strip();

			switch (key)
			{
				case "name": rhs.name = val; break;
				case "type": rhs.type = val; break;
				case "maintainer": rhs.maintainer = val; break;
				case "version": rhs.edition = val; break;
				default: if (strict) throw new ParserError("Invalid data on line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line)); break;
			}
		}
		else
			if (strict) throw new ParserError("Malformed line " ~ to!string(lineNo) ~ "\n-> " ~ to!string(line));
	}

	return rhs;
}

