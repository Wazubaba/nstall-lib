module nstall.database;

import std.file;
import std.path;
import std.conv: to;
import std.ascii: newline;
import pkg;
import dwst.config: Config;

/++
	Contains functions for interacting with the nstall database.
	Each entry is a directory named after the package and contains
	both the info and desc files, along with a manifest file, which
	will store the absolute path of each installed file.
++/
debug
{
	enum DEFAULTSETTINGS = [
		"package storage directory": "./testdatabase/installed-packages",
		"manifest storage directory": "./testdatabase/manifest"
];
}
else
{
	enum DEFAULTSETTINGS = [
		"package storage directory": "/var/cache/nstall/installed-packages",
		"manifest storage directory": "/var/cache/nstall/manifest"
	];
}

/++
	Add relevant files and data to the database of installed packages.
	Takes:
		Package pkg - package to register
		bool silent = false - whether to omit informative output
		Config cfg = DEFAULTSETTINGS - optional configuration to use
	Throws:
		TBD - function is not yet complete :P FIXME
++/
void Register(Package pkg, bool silent = false, Config cfg = DEFAULTSETTINGS)
{
	/*
		Make a directory in `/var/cache/nstall/installed-packages` (use mkdir -p style)
		Make a directory in `/var/cache/nstall/local-packages` (use mkdir -p style)

		extract .pkg into /tmp
	*/

	import std.stdio: writeln;

	string packagePath;
	string manifestPath;

	if ("package storage directory" in cfg)
		packagePath = cfg["package storage directory"];
	else // This is for if the Config lacks the settings for the paths
		packagePath = buildPath(DEFAULTSETTINGS["package storage directory"], pkg.info.name);

	if ("manifest storage directory" in cfg)
		manifestPath = buildPath(cfg["manifest storage directory"], pkg.info.name);
	else // ditto
		manifestPath = buildPath(DEFAULTSETTINGS["manifest storage directory"], pkg.info.name);

	writeln("[nstall-debug]: using '", packagePath, "' for packages");
	writeln("[nstall-debug]: using '", manifestPath, "' for manifests");

	// Generate the storage directories if they do not already exist
	if (!packagePath.exists) mkdirRecurse(packagePath);
	if (!manifestPath.exists) mkdirRecurse(manifestPath);

	debug writeln("[nstall-debug]: Ensured required paths exist");

//	pkg.Export(packagePath);

	string manifestData;
	foreach (line; pkg.manifest)
		manifestData ~= line ~ newline;

	write(buildPath(manifestPath, pkg.info.name ~ ".mnfst"), manifestData);
	writeln("[nstall]: Installing package: '" ~ pkg.info.name ~ "'...");

}

/++
	Remove the data stored for a given package from the database.
	Takes:
		string pkgName - name of the package to delete
		bool silent = false - whether to omit informative output
		Config cfg = DEFAULTSETTINGS - optional configuration to use
	Throws:
		TBD - function is not yet complete :P FIXME
++/
void Unregister(string pkgName, bool silent = false, Config cfg = DEFAULTSETTINGS)
{
}

unittest
{
	import std.stdio: writeln;
	writeln("== Begin manifest.d unittests ==");
	Package pkg;
	pkg.Import("../nstall-package.wtar.gz");
	Register(pkg);
	writeln("=== End manifest.d unittests ==");
}

