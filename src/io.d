module nstall.io;

import dwst.wtar: ArchivalData;

/++
	Contains numerous things that the various parts of the system depend upon.
++/

/++
	Generate a guarenteed unique temporary directory to work in and return the
	path to it.

	Takes:
		string prefix - prefix for the newly generated path
		size_t randomSize = 10 - optional control for how large the random path should be
	Returns: string containing a valid temporary directory
++/
string GenerateTempDir(string prefix, size_t randomSize = 10)
{
	import std.path: buildPath;
	import std.file: exists, tempDir;

	string _GenString(size_t size)
	{
		import std.random: Random, unpredictableSeed, uniform;
		import std.conv: to;

		char[] buffer;
		auto gen = Random(unpredictableSeed);
		for (size_t i; i < size; i++)
		{
			buffer ~= cast(char) uniform(97, 122);
		}

		return to!string(buffer);
	}

	string path;

	while ((path = buildPath(tempDir(), prefix ~ "-" ~ _GenString(randomSize))).exists())
	{}

	return path;
}
