module nstall.pkg;

/++
	Implements a container for an Nstall Package.
++/

import dwst.wtar: ArchivalData;
import info;

/// Thrown if an opened package is invalid
class InvalidPackageException: Exception
{
	@safe nothrow this(string archiveName)
	{
		string errmsg = "Package '" ~ archiveName ~ "' is not a valid nstall package.";
		super(errmsg);
	}
}

/// A valid nstall package
struct Package
{
	string desc;
	Info info;
	string[] manifest;

	ArchivalData data;

	/++
		Import a target package's data for processing.
		Takes:
			string path - path to the package
			bool silent - whether to omit output of information
		Throws:
			InvalidPackageException - target package is not valid
			InflationError - see wtar documentation for archive.Inject
	++/
	void Import(string path, bool silent = false)
	{
		import std.file: read;
		import std.zlib: uncompress;
		import std.conv: to;
		import std.path: buildPath;

		auto archive = new ArchivalData();
		archive.Inject(to!string(uncompress(read(path))));

		string basename = archive.data[0].path;

		foreach (file; archive.data)
		{
			if (file.path == buildPath(basename, ".nstall-meta/desc"))
				this.desc = to!string(file.data);
			else
			if (file.path == buildPath(basename, ".nstall-meta/info"))
				this.info = to!string(file.data).ParseInfoData();
			else
				this.manifest ~= file.path;
		}

		if (!this.desc.length > 0 || !this.info.name.length > 0)
		{
			if (!silent)
			{
				import std.stdio: writefln;
				writefln("[nstall]: package '%s' is not a valid nstall package.", path);
			}

			throw new InvalidPackageException(path);
		}

		if (!silent)
		{
			import std.stdio: writefln;
			writefln("[nstall]: opened package %s..", path);
			debug writefln("[nstall]: package %s info:\n%s\ndesc:\n%s", path, info, desc);
		}
	}

	/++
		Export an nstall package to a wtar.gz archive.
		Takes:
			string path - path to export the package to
			bool silent - whether to omit information output
		Throws:
			ScanError - see wtar documentation for ArchivalData.ScanTargetDirectory
	++/
	void Export(string path, bool silent = false)
	{
		import std.file: write;
		import std.zlib: compress;

		auto arcdata = compress(this.data.Serialize());
		write(path, arcdata);

		if (!silent)
		{
			import std.stdio: writefln;
			writefln("[nstall]: Exported package '%s'", path);
		}
	}
}

/++
	Unpack a target into a Package struct and return it.
	Takes:
		string path - path to the target package
	Returns:
		Package rhs - imported package data
	Throws:
		InvalidPackageException if target package is not valid
++/
Package UnpackPackage(string path)
{
	Package rhs;
	rhs.Import(path);
	return rhs;
}

/++
	Test whether a path exists and is a file.
	Takes:
		string path - path to test
	Returns:
		bool result - whether the test succeeded or not
++/
bool isFileAndExists(string path)
{
	import std.file: exists, isFile;
	return path.exists() && path.isFile();
}

/++
	Create an nstall package from a target directory.
	Takes:
		string path - path to the package source
		string dest - where to export the package to
		bool silent - whether to omit information output
	Throws:
		InvalidPackageException - the target directory is missing meta package information
		ScanError - see wtar documentation for ArchivalData.ScanTargetDirectory
++/
void CreatePackage(string path, string dest, bool silent = false)
{
	import std.file: write;
	import std.zlib: compress;
	import std.path: buildPath;
	import std.file: exists;
	import std.file: isFile;

	if (!buildPath(path, ".nstall-meta/info").isFileAndExists() || !buildPath(path, ".nstall-meta/desc").isFileAndExists())
		throw new InvalidPackageException(path);

	string pkgdata = new ArchivalData(path).Serialize();
	auto arcdata = compress(pkgdata);
	write(dest, arcdata);

	if (!silent)
	{
		import std.stdio: writefln;
		writefln("[nstall]: Created package '%s' for '%s'", dest, path);
	}
}

unittest
{
	import std.stdio: writeln;
	import std.file: exists;

	writeln("=== Begin pkg.d unittests ===");

	if (!"../nstall-package.wtar.gz".exists())
	{
		writeln("Please move a valid archive named 'nstall-package.wtar.gz' into the directory above the current working directory");
	} else
	{
		auto pkg = new Package();
		pkg.Import("../nstall-package.wtar.gz");
	}

	writeln("=== End pkg.d unittests ===");
}
