DC= dmd
DFLAGS=

OUTNAME= nstall
PACKAGE= nstall

INCLUDES= -Isupport/libwtar/include -Isupport/libconfig/include -Isrc

i386-LIB-INCLUDE= -L-Lsupport/libwtar/i386 -L-Lsupport/libconfig/i386
x86_64-LIB-INCLUDE= -L-Lsupport/libwtar/x86_64 -L-Lsupport/libconfig/x86_64

lib-release= -L-lwtar -L-lconfig
lib-debug= -L-lwtar-debug -L-lconfig-debug

i386-libpath-release= support/libwtar/i386/libwtar.a support/libconfig/i386/libconfig.a
x86_64-libpath-release= support/libwtar/x86_64/libwtar.a support/libconfig/x86_64/libconfig.a

i386-libpath-debug= support/libwtar/i386/libwtar-debug.a support/libconfig/i386/libconfig-debug.a
x86_64-libpath-debug= support/libwtar/x86_64/libwtar-debug.a support/libconfig/x86_64/libconfig-debug.a

SRCFILES= $(shell find src -type f ! -name "*skip.*" -and ! -path "*skip.*" -name "*.d")
i386-release-objfiles= $(patsubst %.d, i386/obj-release/%.o, $(SRCFILES))
i386-debug-objfiles= $(patsubst %.d, i386/obj-debug/%.o, $(SRCFILES))
i386-test-objfiles= $(patsubst %.d, i386/obj-test/%.o, $(SRCFILES))

x86_64-release-objfiles= $(patsubst %.d, x86_64/obj-release/%.o, $(SRCFILES))
x86_64-debug-objfiles= $(patsubst %.d, x86_64/obj-debug/%.o, $(SRCFILES))
x86_64-test-objfiles= $(patsubst %.d, x86_64/obj-test/%.o, $(SRCFILES))

headers= $(addprefix include/$(PACKAGE)/, $(notdir $(patsubst %.d, includes/$(PACKAGE)/%.di, $(SRCFILES))))
docpaths= $(addprefix docs/$(PACKAGE)/, $(notdir $(patsubst %.d, docs/$(PACKAGE)/%.html, $(SRCFILES))))


.PHONY: release debug test clean all package

release: release32 release64
release32: $(i386-libpath-release) i386/lib$(OUTNAME).a $(headers)
release64: $(x86_64-libpath-release) x86_64/lib$(OUTNAME).a $(headers)

debug: debug32 debug64
debug32: $(i386-libpath-debug) i386/lib$(OUTNAME)-debug.a $(headers)
debug64: $(x86_64-libpath-debug) x86_64/lib$(OUTNAME)-debug.a $(headers)

test: test32 test64
test32: $(i386-libpath-debug) i386/lib$(OUTNAME)-test
test64: $(x86_64-libpath-debug) x86_64/lib$(OUTNAME)-test

docs: $(docpaths)

package: release/lib$(OUTNAME)-release.tar.gz release/lib$(OUTNAME)-debug.tar.gz release/lib$(OUTNAME)-source.tar.gz release/lib$(OUTNAME)-release.zip release/lib$(OUTNAME)-debug.zip release/lib$(OUTNAME)-source.zip
all: release debug test docs

#### LIBRARY BUILD RULES ####
## libwtar
support/libwtar/i386/libwtar-debug.a:
	@cd support/libwtar; $(MAKE) debug32

support/libwtar/i386/libwtar.a:
	@cd support/libwtar; $(MAKE) release32

support/libwtar/x86_64/libwtar-debug.a:
	@cd support/libwtar; $(MAKE) debug64

support/libwtar/x86_64/libwtar.a:
	@cd support/libwtar; $(MAKE) release64

## libconfig
support/libconfig/i386/libconfig-debug.a:
	@cd support/libconfig; $(MAKE) debug32

support/libconfig/i386/libconfig.a:
	@cd support/libconfig; $(MAKE) release32

support/libconfig/x86_64/libconfig-debug.a:
	@cd support/libconfig; $(MAKE) debug64

support/libconfig/x86_64/libconfig.a:
	@cd support/libconfig; $(MAKE) release64



#### COMPILING RULES ####

#####################
# 32bit Compile rules
#####################

### Object builders ###
i386/obj-release/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -release -O -of$@ $<

i386/obj-debug/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -g -debug -of$@ $<

i386/obj-test/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m32 -c -g -debug -unittest -main -of$@ $<

### Lib builders ###
i386/lib$(OUTNAME).a: $(i386-release-objfiles)
	@mkdir -p i386
	$(DC) $(DFLAGS) $(i386-libpath-release) -m32 -lib -release -O -of$@ $<

i386/lib$(OUTNAME)-debug.a: $(i386-debug-objfiles)
	@mkdir -p i386
	$(DC) $(DFLAGS) $(i386-libpath-debug) -m32 -lib -g -debug -of$@ $<

### Unittest builder ###
i386/lib$(OUTNAME)-test: $(i386-test-objfiles)
	@mkdir -p i386
	$(DC) $(DFLAGS) $(i386-libpath-debug) -m32 -g -debug -unittest -of$@ $<

#####################
# 64bit Compile rules
#####################

### Object Builders ###
x86_64/obj-release/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -release -O -of$@ $<

x86_64/obj-debug/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -g -debug -of$@ $<

x86_64/obj-test/%.o: %.d
	$(DC) $(DFLAGS) $(INCLUDES) -m64 -c -g -debug -unittest -of$@ $<

### Lib builders ###
x86_64/lib$(OUTNAME).a: $(x86_64-release-objfiles)
	@mkdir -p x86_64
	$(DC) $(DFLAGS) $(x86_64-libpath-release) -m64 -lib -release -O -of$@ $+

x86_64/lib$(OUTNAME)-debug.a: $(x86_64-debug-objfiles)
	@mkdir -p x86_64
	$(DC) $(DFLAGS) $(x86_64-libpath-debug) -m64 -lib -g -debug -of$@ $+

### Unittest builder ###
x86_64/lib$(OUTNAME)-test: $(x86_64-test-objfiles)
	@mkdir -p x86_64
	$(DC) $(DFLAGS) $(x86_64-libpath-debug) -m64 -g -debug -unittest -main -of$@ $+

###############################################################################

# This does not work because we need the path to the target file, not just a list of all files
### Header file generation ###
include/$(PACKAGE)/%.di: $(SRCFILES)
	@mkdir -p include/$(PACKAGE)
	$(DC) $(DFLAGS) $(INCLUDES) -o- -H -Hf$@ $(addprefix src/, $(notdir $(basename $@))).d
#	$(DC) $(DFLAGS) $(INCLUDES) -o- -H -Hf$@ $<

### Documentation generation ###
docs/%.html: $(SRCFILES)
	@mkdir -p docs/$(PACKAGE)
#	$(DC) $(DFLAGS) -o- -D -Df$@ $<
	$(DC) $(DFLAGS) $(INCLUDES) -o- -D -Df$@ $(addprefix src/, $(notdir $(basename $@))).d

###############################
# PACKAGE CONSTRUCTION RULES
###############################
.lib$(OUTNAME)-release-package: release docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.lib$(OUTNAME)-debug-package: debug docs
	@mkdir -p $@
	@cp -r i386 $@/.
	@cp -r x86_64 $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

.lib$(OUTNAME)-source-package: docs include/$(PACKAGE)/$(OUTNAME).di
	@mkdir -p $@
	@cp -r src $@/.
	@cp -r docs $@/.
	@cp -r include $@/.
	@cp README.md $@/.

release/lib$(OUTNAME)-release.tar.gz: .lib$(OUTNAME)-release-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-release.tar
	@gzip release/lib$(OUTNAME)-release.tar

release/lib$(OUTNAME)-debug.tar.gz: .lib$(OUTNAME)-debug-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-debug.tar
	@gzip release/lib$(OUTNAME)-debug.tar

release/lib$(OUTNAME)-source.tar.gz: .lib$(OUTNAME)-source-package
	@mkdir -p release
	@tar c $< > release/lib$(OUTNAME)-source.tar
	@gzip release/lib$(OUTNAME)-source.tar

release/lib$(OUTNAME)-release.zip: .lib$(OUTNAME)-release-package
	@mkdir -p release
	@zip $@ -r $<

release/lib$(OUTNAME)-debug.zip: .lib$(OUTNAME)-debug-package
	@mkdir -p release
	@zip $@ -r $<

release/lib$(OUTNAME)-source.zip: .lib$(OUTNAME)-source-package
	@mkdir -p release
	@zip -r $@ $<

###############################################################################

#### UTILITY RULES ####
clean:
	-@$(RM) -r i386 x86_64
	-@$(RM) -r include docs
	@cd support/libwtar; $(MAKE) clean
	@cd support/libconfig; $(MAKE) clean
	-@$(RM) -r release .lib$(OUTNAME)-release-package .lib$(OUTNAME)-debug-package .lib$(OUTNAME)-source-package

install: release docs
	@mkdir -p $(DESTDIR)/usr/lib/i386-linux-gnu/.
	@cp i386/lib$(OUTNAME).a $(DESTDIR)/usr/lib/i386-linux-gnu/.

	@mkdir -p $(DESTDIR)/usr/lib/x86_64-linux-gnu/.
	@cp x86_64/lib$(OUTNAME).a $(DESTDIR)/usr/lib/x86_64-linux-gnu/.

	@mkdir -p $(DESTDIR)/usr/include/$(PACKAGE)
	@cp include/$(PACKAGE)$(OUTNAME).di $(DESTDIR)/usr/include/$(PACKAGE)/.

	@mkdir -p $(DESTDIR)/usr/share/doc/$(PACKAGE)/libwtar
	@cp docs/$(OUTNAME).html $(DESTDIR)/usr/share/doc/$(PACKAGE)/lib$(OUTNAME)/.

uninstall:
	$(RM) $(DESTDIR)/usr/lib/i386-linux-gnu/lib$(OUTNAME).a
	$(RM) $(DESTDIR)/usr/lib/x86_64-linux-gnu/lib$(OUTNAME).a
	$(RM) $(DESTDIR)/usr/include/$(PACKAGE)/$(OUTNAME).di
	$(RM) -r $(DESTDIR)/usr/share/doc/$(PACKAGE)/lib$(OUTNAME)
